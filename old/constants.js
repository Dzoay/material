export default {
  percentageChildrenU1 : 1.2/5,
  percentageChildrenU2 : 2.1/5,
  daysInYear: 365,
  monthsInYear: 12,
  meanGestationPeriod : 280,
  perThousand: 1000,
  percentageRequireBEmONC: 0.01,
  percentageRequireCEmONC: 0.02,
  percentageEpidemicSGBV: 0.01,
  percentageEndemicSGBV: 0.005,
  childU5ConsultationsPerYear: 2,
  adultConsultationsPerYear: 1,
  percentageVCTTestPerMonth: 0.05,
  odd: 30.42
};
