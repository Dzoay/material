(function () {
	/* globals define, math */

	function mathJsModule() {
		'use strict';

		return { 'default': math };
	}

	define('mathjs', [], mathJsModule);

})();