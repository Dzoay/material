export default {
  // "some.translation.key": "Text for some.translation.key",
  //
  // "a": {
  //   "nested": {
  //     "key": "Text for a.nested.key"
  //   }
  // },
  //
  // "key.with.interpolation": "Text with {{anInterpolation}}"

  constants: {
    title: "Constantes disponibles",
    add: {
      title: "Nouveau titre constant",
      value: "Nouvelle valeur constante"
    }
  },

  settings: {
    title: "Paramètres de l'application",
    "supported-languages": {
      title: "Langues supportées"
    }
  },

  errors: {
    inclusion: "ne sont pas inclus dans la liste",
    exclusion: "est réservée",
    invalid: "est invalide",
    confirmation: "ne correspond pas {{attribute}}",
    accepted: "doit être accepté",
    empty: "ne peut pas être vide",
    blank: "ne peut être vide",
    present: "doit être vide",
    tooLong: "est trop long (un maximum de {{count}} caractères)",
    tooShort: "est trop court (minimum est {{count}} caractères)",
    wrongLength: "est la bonne longueur (devrait être {{count}} caractères)",
    notANumber: "est pas un nombre",
    notAnInteger: "doit être un entier",
    greaterThan: "doit être supérieure à {{count}}",
    greaterThanOrEqualTo: "doit être supérieur ou égal à {{count}}",
    equalTo: "doit être égal à {{count}}",
    lessThan: "doit être inférieure à {{count}}",
    lessThanOrEqualTo: "doit être inférieure ou égale à {{count}}",
    otherThan: "doit être autre que {{count}}",
    odd: "doit être impair",
    even: "doit être encore"
  }
};
