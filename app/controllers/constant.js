import Ember from 'ember';

export default Ember.Controller.extend({

  isEditing: false,

  actions: {
    edit: function () {
      this.set('isEditing', true);
    },

    remove: function () {
      var todo = this.get('model');
      todo.deleteRecord();
      todo.save();
    },

    acceptChanges: function () {
      this.set('isEditing', false);
      this.get('model').save();
    }
  }
});
