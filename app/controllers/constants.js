import Ember from 'ember';
import EmberValidations from 'ember-validations';
import { translationMacro as t } from "ember-i18n";

export default Ember.Controller.extend(EmberValidations, {
  i18n: Ember.inject.service(),

  constantsTitle: t('constants.title'),
  newConstantTitle: t('constants.add.title'),
  newConstantValueTitle: t('constants.add.value'),

  validations: {
    title: {
      presence: true,
      length: { minimum: 5 },
      format: { with: /^([a-zA-Z]|\d)+$/, allowBlank: true, message: 'must be letters and numbers only'  }
    },
    value: {
      numericality: true
    },
    profile: true
  },

  actions: {
    createConstant: function () {
      var store = this.store;

      console.log('this is in the create constant function');

      var title = this.get('title');
      var value = this.get('value');

      var constant = store.createRecord('constant', {
        title: title,
        value: value
      });

      constant.save();
    }
  }
});
