import Ember from 'ember';
import { translationMacro as t } from "ember-i18n";

export default Ember.Controller.extend({
  i18n: Ember.inject.service(),

  settingsTitle: t("settings.title"),
  supportedLanguagesTitle: t('settings.supported-languages.title'),

  availableLanguages: function(){
    var locales = this.get('i18n.locales');
    console.log(locales);
    return this.get('i18n.locales');
  }.property()

});
