import DS from 'ember-data';
import { Model } from 'ember-pouch';

export default Model.extend({
  availableLocales: DS.hasMany('locale'),
  selectedLocale: DS.belongsTo('locale')
});
