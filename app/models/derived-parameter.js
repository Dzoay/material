import DS from 'ember-data';
import { Model } from 'ember-pouch';

export default Model.extend({
	key: DS.attr('string'),
	title: DS.attr('string'),
	expression: DS.attr('string'),
	parameters: DS.hasMany('number-parameter')
});
