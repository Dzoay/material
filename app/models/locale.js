import DS from 'ember-data';
import { Model } from 'ember-pouch';

export default Model.extend({
  name: DS.attr('string'),
  locale: DS.attr('string'),
  flag: DS.attr('string')
});
