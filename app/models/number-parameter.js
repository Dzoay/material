import DS from 'ember-data';
import { Model } from 'ember-pouch';

export default Model.extend({
	key: DS.attr('key'),
	title: DS.attr('string'),
	value: DS.attr('number')
});
