import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('about', { path: '/about' });
  this.route('favorites', { path: '/favs' });
  this.route('login');
  this.route('intervention-modules');
  this.route('constants');
  this.route('parameters');
  this.route('expressions');
  this.route('settings');
});

export default Router;
