import Ember from 'ember';

export default Ember.Component.extend({
  tagName: 'li',
  classNames: ['collection-item', 'valign-wrapper'],

  actions: {
    edit() {
      this.set('isEditing', true);
    },

    remove() {
      var item = this.get('item');

      item.deleteRecord();
      item.save();
    },

    save() {
      this.set('isEditing', false);
      this.get('item').save();
    }
  }
});
