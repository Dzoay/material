import Ember from 'ember';
import Math from 'mathjs';

export default Ember.Component.extend({
  symbols: function() {
    var expression = this.get('expression');
    var node = Math.parse(expression);
    var filtered = node.filter(function (node) {
      return node.type === 'SymbolNode' && node.name === 'x';
    });
  }.property('expression')
});
