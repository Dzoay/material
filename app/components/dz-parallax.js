import Ember from 'ember';

export default Ember.Component.extend({
	classNames: ['parallax-container'],

	didInsertElement() {
		this._super(...arguments);
		this._setupParallax();
	},

	_setupParallax() {
		this.$('.parallax').parallax();
	},

	//TODO: unregister any listeners that $.parallax() registers
	// _teardownParallax() {
	//
	// }
});
