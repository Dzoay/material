import Ember from 'ember';

export default Ember.Route.extend({
  model: function() {
    var locales = this.get('i18n.locales');
    console.log("these are the locales available");
    console.log(locales);

    return {
      locales: locales
    };
  }
});
