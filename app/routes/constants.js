import Ember from 'ember';

export default Ember.Route.extend({
  queryParams: {
    state: { refreshModel: true }
  },

  model(params) {
    return this.store.findAll('constant').then((constants) => {
      return {
        all: constants,
        filter: params.state
      };
    });
  }
});
